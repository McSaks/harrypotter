import numpy as np
from scipy.optimize import curve_fit


def gauss_normalized(x, μ, σ):
  return np.exp(-((x - μ) / σ)**2 / 2) / (np.sqrt(2 * np.pi) * σ)


def gauss_scaled(x, μ, σ, A):
  return A * np.exp(-((x - μ) / σ)**2 / 2) / (np.sqrt(2 * np.pi) * σ)


def fit_gauss(vals, initial_guess=None, *, bins=100, scaled=False):
  """Return (mu, sigma) via gauss fit."""

  if initial_guess is None:
    initial_guess = np.mean(vals), np.std(vals)

  hist_values, bin_edges = np.histogram(vals, bins=bins, density=True)
  bin_centers = (bin_edges[:-1] + bin_edges[1:]) / 2

  if scaled:
    coeff, var_matrix = curve_fit(gauss_scaled, bin_centers, hist_values, p0=initial_guess + (1,))
  else:
    coeff, var_matrix = curve_fit(gauss_normalized, bin_centers, hist_values, p0=initial_guess)

  return tuple(coeff)
