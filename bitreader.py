#!/usr/bin/env python3

class bitreader(object):
  def __init__(self, filename):
    self.file = open(filename, 'rb')
    self.bits = self.bitstream(self.file)

  def __del__(self):
    self.file.close()

  def read(self):
    try:
      return self.bits.__next__()
    except StopIteration:
      return None

  # http://stackoverflow.com/a/2577487
  @staticmethod
  def bitstream(f):
    bytes = (b for b in f.read())
    for b in bytes:
      for i in range(8):
        yield (b >> i) & 1
