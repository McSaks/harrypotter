#!/usr/bin/env python3

from bitreader import bitreader
from HarryPyther import *
import sys

def disp(x, pre = ''):
  print(pre, x)
  return x

good = 0
total = 0

import argparse
parser = argparse.ArgumentParser()
parser.add_argument("mask",
                    metavar = "MASKFILE",
                    type = str,
                    help = "file with binary mask created by AngRes -m MASKFILE")
parser.add_argument("file",
                    metavar = "INFILE",
                    type = str,
                    help = "file to process")
parser.add_argument("ofile",
                    metavar = "OUTFILE",
                    type = str,
                    help = "file to write to")
parser.add_argument("-n",
                    "--nevents",
                    metavar = "N",
                    type = int,
                    default = None,
                    help = "constrain number of events to process to N")
args = parser.parse_args()
print('Passed args are:', vars(args), sep = '\n', end = '\n\n', file = sys.stderr)
locals().update(args.__dict__)


with HarryPyther(file, nevents, ofile = ofile) as hp:
  
  hp.flush()

  masker = bitreader(mask)
  
  while hp.next():
    
    if not hp:
      raise Exception('Error in Read')
    total += 1
    
    trigval = masker.read()
    print('● ' if trigval else '∘ ', end = '', file = sys.stderr)
    if not hp.event % 10:
      print('event # {:,}'.format(hp.event), file = sys.stderr)
    sys.stderr.flush()
    if not trigval: continue
    
    #print("[[[", hp._buffer, "]]]")
    hp.flush()
    good += 1

print('', file = sys.stderr)
print('{good:,} / {total:,} = {fraction} %'.format(
    good = good,
    total = total,
    fraction = good/total * 100 if total else 0
  ))
