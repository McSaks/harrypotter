#!/usr/bin/env python3

from HarryPyther import *
import sys
import numpy as np

def disp(x, pre = ''):
  print(pre, x)
  return x

good = 0
total = 0

import argparse
parser = argparse.ArgumentParser(
  description = '*******.',
  fromfile_prefix_chars = '@',
)
parser.add_argument("file",
                    metavar = "EVENTDATA",
                    type = str,
                    help = "file to process.")
parser.add_argument("-n",
                    "--nevents",
                    metavar = "N",
                    type = int,
                    default = None,
                    help = "constrain number of events to process to N.")
parser.add_argument("-t",
                    "--threshold",
                    metavar = "VALUE_IN_MeV",
                    type = float,
                    default = 0.0,
                    help = "threshold to be used in AC and ToF detectors.")
args = parser.parse_args()
print('Run with arguments: ', str(args)[10 : -1], file = sys.stderr, sep = '')
locals().update(args.__dict__)


def get_length(detector, nlayer):
  axis = detector.along_axis[nlayer]
  return getattr(detector, 'd' + axis)[nlayer]


def get_time(detector, nlayer):
  #S_AXIS, T_AXIS, P_AXIS = range(3)
  data = np.array(detector.get_array()[nlayer])
  tmin = detector.tmin[nlayer]
  tmax = detector.tmax[nlayer]
  tbins = detector.tbins[nlayer]
  pbins = detector.pbins[nlayer]
  length = get_length(detector, nlayer)
  dt = (tmax - tmin) / tbins
  dp = length / pbins
  speed_of_light = 0.5 * 299.792458 # mm/ns

  def get_t(t):
    return tmin + (t + 1/2) * dt

  def get_p(p):
    return (p + 1/2) * dp

  def get_t1(t, p):
    return get_t(t) + get_p(p) / speed_of_light

  def get_t2(t, p):
    return get_t(t) + (length - get_p(p)) / speed_of_light

  def get_time_dE_pairs(tp_subarray, get_ti):
    enumerated = np.ndenumerate(tp_subarray) # enumerated = iterator[ [(t0, p0), dE0], [(t1, p1), dE1], ... ]
    time_dE_pairs = [
      [get_ti(t, p), dE]
      for [(t, p), dE] in enumerated
      if dE > 0
    ]
    time_dE_pairs.sort(key = lambda pair: pair[0]) # sort by time
    return time_dE_pairs

  def get_time_in_strip(time_dE_pairs, threshold):
    total_E = 0.0
    for time, dE in time_dE_pairs:
      total_E += dE
      if total_E >= threshold:
        return time
    else:
      import math
      return math.inf

  t1_dE_pairs = np.array([get_time_dE_pairs(tp_subarray, get_t1) for tp_subarray in data])
  t2_dE_pairs = np.array([get_time_dE_pairs(tp_subarray, get_t2) for tp_subarray in data])

  t1s = [get_time_in_strip(strip, threshold) for strip in t1_dE_pairs]
  t2s = [get_time_in_strip(strip, threshold) for strip in t2_dE_pairs]

  times = [(t1 + t2 - length/speed_of_light)/2 for (t1, t2) in zip(t1s, t2s)]

  return min(times)


def get_min_time(detector):
  times = (get_time(detector, ilayer) for ilayer in range(detector.nlayers))
  return min(times)


for hp in HarryPyther(file, nevents, ofile='/dev/null'):

  if not hp:
    raise Exception('Error in Read')

  t_AC = get_min_time(hp.topAC)
  t_S1 = get_min_time(hp.S1)

  print(t_AC - t_S1)

  if hp.event % 10:
    print('. ', end = '', file = sys.stderr)
  else:
    print('event # {:,}'.format(hp.event), file = sys.stderr)
  total += 1

print('', file = sys.stderr)
